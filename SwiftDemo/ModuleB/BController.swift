//
//  BController.swift
//  SwiftDemo
//
//  Created by huangj on 2018/10/29.
//  Copyright © 2018 Hangzhou Zhongheng Cloud Energy Internet Technology Co., Ltd. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


class BController: UIViewController {
    
    let disposeBag = DisposeBag()
    var tableview:UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        let button = UIButton.init(type: .custom)
//        button.frame = CGRect.init(x: 0, y: 100, width: 100, height: 40)
//        self.view .addSubview(button)
//        button.backgroundColor = UIColor.red
//
////        button.rx.tap.subscribe({_ in
////            self.showMessage("按钮被点击")
////        })
////       .disposed(by: disposeBag)
//        button.rx.tap.bind { _ in
//            self.showMessage("click Button")
//        }.disposed(by: disposeBag)
        
        self.tableview = UITableView(frame: self.view.frame, style:.plain)
        //创建一个重用的单元格
        self.tableview!.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        self.view.addSubview(self.tableview!)
        
        //初始化数据
        let items = Observable.just([
            "文本输入框的用法",
            "开关按钮的用法",
            "进度条的用法",
            "文本标签的用法",
            ])
        
        //设置单元格数据（其实就是对 cellForRowAt 的封装）
        items
            .bind(to: tableview.rx.items) { (tableView, row, element) in
                let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")!
                cell.textLabel?.text = "\(row)：\(element)"
                return cell
            }
            .disposed(by: disposeBag)
        
//        tableview.rx.modelSelected(String.self).subscribe(onNext: { (item) in
//            print(item)
//        }).disposed(by: disposeBag)
        
        tableview.rx.itemSelected.subscribe(onNext: { indexPath in
            print("选中项的indexPath为：\(indexPath)")
        }).disposed(by: disposeBag)
        
    }
    func showMessage(_ text: String) {
        let alertController = UIAlertController(title: text, message: nil, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "确定", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
