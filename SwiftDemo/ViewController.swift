//
//  ViewController.swift
//  SwiftDemo
//
//  Created by huangj on 2018/10/26.
//  Copyright © 2018 Hangzhou Zhongheng Cloud Energy Internet Technology Co., Ltd. All rights reserved.
//

import UIKit
import CTMediator
import RxSwift
import RxCocoa
import XLPagerTabStrip
import SnapKit
import TTADataPickerView

class ZHElectricityBillStatisticsViewController: ButtonBarPagerTabStripViewController {
    var isReload = false
    let blueInstagramColor = UIColor(red: 240/255.0, green: 131/255.0, blue: 0/255.0, alpha: 1)
    
    override func viewDidLoad() {
        
        settings.style.buttonBarBackgroundColor = .white
        settings.style.buttonBarItemBackgroundColor = .white
        settings.style.selectedBarBackgroundColor = blueInstagramColor
        settings.style.buttonBarItemFont = .boldSystemFont(ofSize: 14)
        settings.style.selectedBarHeight = 2.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .black
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        
        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = UIColor(red:51/255.0, green:51/255.0, blue:51/255.0, alpha:1.0)
            newCell?.label.textColor = self?.blueInstagramColor
        }
        super.viewDidLoad()
        
        //        self.navigationItem.title = "电费统计"
        //        let button = UIButton.init(type: .custom)
        //        button .setImage(UIImage(named: "back"), for: .normal)
        //        let barButtonItem = UIBarButtonItem.init(customView: button)
        //        self.navigationItem.backBarButtonItem = barButtonItem
    }
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        
        let child_2 = ChildExampleViewController(itemInfo: "月账单")
        let child_4 = ChildExampleViewController(itemInfo: "基本电费")
        let child_6 = ChildExampleViewController(itemInfo: "电度电费")
        let child_8 = ChildExampleViewController(itemInfo: "力调电费")
        //        guard isReload else {
        //            return [ child_2,  child_4, child_6, child_8]
        //        }
        
        var childViewControllers = [ child_2,  child_4, child_6, child_8]
        
        for index in childViewControllers.indices {
            let nElements = childViewControllers.count - index
            let n = (Int(arc4random()) % nElements) + index
            if n != index {
                childViewControllers.swapAt(index, n)
            }
        }
        let nItems = 1 + (arc4random() % 8)
        return Array(childViewControllers.prefix(Int(nItems)))
    }
    
}


class ChildExampleViewController: UIViewController, IndicatorInfoProvider {
    
    let disposeBag = DisposeBag()
    let dateFormatter = DateFormatter()
    var itemInfo: IndicatorInfo = "月账单"
    
    init(itemInfo: IndicatorInfo) {
        self.itemInfo = itemInfo
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var titleLabel:UILabel!
    var lineLabel:UILabel!
    var pickerViewButton:UIButton!
    
    var currentMonthValueLabel:UILabel!
    var lastMonthValueLabel:UILabel!
    var currentMonthDescLabel:UILabel!
    var lastMonthDescLabel:UILabel!
    
    var verticalLineLabel:UILabel!
    var midlineLabel:UILabel!
    var midlineTitleLabel:UILabel!
    var midlineValueLabel:UILabel!
    var bottomlineLabel:UILabel!
    var bottomlineTitleLabel:UILabel!
    var bottomlineValueLabel:UILabel!
    
    var chartTitleLabel:UILabel!
    var chartPickerViewButton:UIButton!
    var chartLineLabel:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MARK: 顶部
        
        titleLabel = UILabel.init()
        self.view .addSubview(titleLabel)
        titleLabel.text = "电费账单（万元）"
        titleLabel.font = UIFont.systemFont(ofSize: 15)
        
        lineLabel = UILabel.init()
        self.view .addSubview(lineLabel)
        lineLabel.text = " "
        lineLabel.backgroundColor = UIColor.init(red: 229/255.0, green: 229/255.0, blue: 229/255.0, alpha: 1)
        
        pickerViewButton = UIButton.init(type: .custom)
        pickerViewButton.setTitle("2019-01", for: .normal)
        pickerViewButton.setTitleColor(UIColor.init(red: 102/255.0, green: 102/255.0, blue: 102/255.0, alpha: 1), for: .normal)
        pickerViewButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        self.view .addSubview(pickerViewButton)
        
        currentMonthValueLabel = UILabel.init()
        self.view .addSubview(currentMonthValueLabel)
        currentMonthValueLabel.text = "3240"
        currentMonthValueLabel.font = UIFont.systemFont(ofSize: 15)
        currentMonthValueLabel.textAlignment = .center
        
        lastMonthValueLabel = UILabel.init()
        self.view .addSubview(lastMonthValueLabel)
        lastMonthValueLabel.text = "3240"
        lastMonthValueLabel.font = UIFont.systemFont(ofSize: 15)
        lastMonthValueLabel.textAlignment = .center
        
        currentMonthDescLabel = UILabel.init()
        self.view .addSubview(currentMonthDescLabel)
        currentMonthDescLabel.text = "本月基本电费"
        currentMonthDescLabel.font = UIFont.systemFont(ofSize: 12)
        currentMonthDescLabel.textColor = UIColor.init(red: 153/255.0, green: 153/255.0, blue: 153/255.0, alpha: 1)
        currentMonthDescLabel.textAlignment = .center
        
        lastMonthDescLabel = UILabel.init()
        self.view .addSubview(lastMonthDescLabel)
        lastMonthDescLabel.text = "上月基本电费"
        lastMonthDescLabel.font = UIFont.systemFont(ofSize: 12)
        lastMonthDescLabel.textColor = UIColor.init(red: 153/255.0, green: 153/255.0, blue: 153/255.0, alpha: 1)
        lastMonthDescLabel.textAlignment = .center
        
        verticalLineLabel = UILabel.init()
        self.view .addSubview(verticalLineLabel)
        verticalLineLabel.text = " "
        
        //MARK: 中部
        midlineLabel = UILabel.init()
        self.view .addSubview(midlineLabel)
        midlineLabel.text = " "
        midlineLabel.backgroundColor = UIColor.init(red: 229/255.0, green: 229/255.0, blue: 229/255.0, alpha: 1)
        
        midlineTitleLabel = UILabel.init()
        self.view .addSubview(midlineTitleLabel)
        midlineTitleLabel.text = "本月核定需量（kW)"
        midlineTitleLabel.font = UIFont.systemFont(ofSize: 14)
        midlineTitleLabel.textColor = UIColor.init(red: 153/255.0, green: 153/255.0, blue: 153/255.0, alpha: 1)
        
        midlineValueLabel = UILabel.init()
        self.view .addSubview(midlineValueLabel)
        midlineValueLabel.text = "30.5"
        midlineValueLabel.font = UIFont.systemFont(ofSize: 16)
        midlineValueLabel.textColor = UIColor.init(red: 153/255.0, green: 153/255.0, blue: 153/255.0, alpha: 1)
        midlineValueLabel.textAlignment = .right
        
        bottomlineLabel = UILabel.init()
        self.view .addSubview(bottomlineLabel)
        bottomlineLabel.text = " "
        bottomlineLabel.backgroundColor = UIColor.init(red: 229/255.0, green: 229/255.0, blue: 229/255.0, alpha: 1)
        
        bottomlineTitleLabel = UILabel.init()
        self.view .addSubview(bottomlineTitleLabel)
        bottomlineTitleLabel.text = "本月最大需量（kW)"
        bottomlineTitleLabel.font = UIFont.systemFont(ofSize: 14)
        bottomlineTitleLabel.textColor = UIColor.init(red: 153/255.0, green: 153/255.0, blue: 153/255.0, alpha: 1)
        
        bottomlineValueLabel = UILabel.init()
        self.view .addSubview(bottomlineValueLabel)
        bottomlineValueLabel.text = "30.5"
        bottomlineValueLabel.font = UIFont.systemFont(ofSize: 16)
        bottomlineValueLabel.textColor = UIColor.init(red: 153/255.0, green: 153/255.0, blue: 153/255.0, alpha: 1)
        bottomlineValueLabel.textAlignment = .right
        
        
        //MARK: 底部charts
        
        chartTitleLabel = UILabel.init()
        self.view .addSubview(chartTitleLabel)
        chartTitleLabel.text = "需量趋势"
        chartTitleLabel.font = UIFont.systemFont(ofSize: 14)
        chartTitleLabel.textColor = UIColor.init(red: 153/255.0, green: 153/255.0, blue: 153/255.0, alpha: 1)
        
        chartPickerViewButton = UIButton.init(type: .custom)
        chartPickerViewButton.setTitle("2019-01", for: .normal)
        chartPickerViewButton.setTitleColor(UIColor.init(red: 102/255.0, green: 102/255.0, blue: 102/255.0, alpha: 1), for: .normal)
        chartPickerViewButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        self.view .addSubview(chartPickerViewButton)
        
        chartLineLabel = UILabel.init()
        self.view .addSubview(chartLineLabel)
        chartLineLabel.text = " "
        chartLineLabel.backgroundColor = UIColor.init(red: 229/255.0, green: 229/255.0, blue: 229/255.0, alpha: 1)
        
        
        //MARK:  适配
        
        titleLabel.snp.makeConstraints { (make) in
            make.top.left.equalTo(15)
            make.bottom.equalTo(lineLabel.snp.top).offset(-15)
            make.height.equalTo(20)
        }
        pickerViewButton.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel)
            make.right.equalTo(-15)
            make.height.equalTo(20)
            make.bottom.equalTo(lineLabel.snp.top).offset(-15)
        }
        lineLabel.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(titleLabel.snp.bottom).offset(15)
            make.right.equalTo(-15)
            make.height.equalTo(1)
        }
        
        currentMonthValueLabel.snp.makeConstraints { (make) in
            make.top.equalTo(lineLabel.snp.bottom).offset(10)
            make.left.equalTo(0)
            make.right.equalTo(verticalLineLabel.snp.left)
            make.bottom.equalTo(currentMonthDescLabel.snp.top)
            make.height.equalTo(20)
            make.width.equalTo(lastMonthValueLabel)
        }
        
        lastMonthValueLabel.snp.makeConstraints { (make) in
            make.top.equalTo(currentMonthValueLabel)
            make.right.equalTo(0)
            make.left.equalTo(verticalLineLabel.snp.right)
            make.bottom.equalTo(currentMonthDescLabel.snp.top).offset(-10)
            make.height.height.equalTo(currentMonthValueLabel)
        }
        
        verticalLineLabel.snp.makeConstraints { (make) in
            make.left.equalTo(currentMonthValueLabel.snp.right)
            make.top.equalTo(lineLabel.snp.bottom).offset(48)
            make.right.equalTo(lastMonthValueLabel.snp.left)
            make.width.equalTo(1)
            make.height.equalTo(30)
        }
        
        
        currentMonthDescLabel.snp.makeConstraints { (make) in
            make.top.equalTo(currentMonthValueLabel.snp.bottom).offset(10)
            make.left.equalTo(0)
            make.right.equalTo(currentMonthValueLabel)
            make.bottom.equalTo(midlineLabel.snp.top)
            make.height.equalTo(20)
            make.width.equalTo(lastMonthDescLabel)
        }
        
        lastMonthDescLabel.snp.makeConstraints { (make) in
            make.top.equalTo(currentMonthDescLabel)
            make.right.equalTo(0)
            make.left.equalTo(lastMonthValueLabel)
            make.bottom.equalTo(midlineLabel.snp.top).offset(-15)
            make.width.height.equalTo(currentMonthDescLabel)
        }
        
        midlineLabel.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(lastMonthDescLabel.snp.bottom).offset(15)
            make.right.equalTo(-15)
            make.height.equalTo(1)
            
        }
        
        midlineTitleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(midlineLabel.snp.bottom).offset(10)
            make.right.equalTo(midlineValueLabel.snp.left)
            make.width.equalTo(midlineValueLabel)
            make.height.equalTo(20)
        }
        
        midlineValueLabel.snp.makeConstraints { (make) in
            make.left.equalTo(midlineTitleLabel.snp.right)
            make.top.equalTo(midlineTitleLabel)
            make.right.equalTo(-15)
            make.width.equalTo(midlineTitleLabel)
            make.height.equalTo(20)
            make.bottom.equalTo(bottomlineLabel.snp.top).offset(-10)
        }
        
        bottomlineLabel.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(midlineValueLabel.snp.bottom).offset(10)
            make.right.equalTo(-15)
            make.height.equalTo(1)
            make.bottom.equalTo(bottomlineTitleLabel.snp.top).offset(-10)
        }
        
        bottomlineTitleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(bottomlineLabel.snp.bottom).offset(10)
            make.left.equalTo(15)
            make.right.equalTo(bottomlineValueLabel.snp.left)
            make.width.equalTo(bottomlineValueLabel)
            make.height.equalTo(20)
        }
        
        bottomlineValueLabel.snp.makeConstraints { (make) in
            make.top.equalTo(bottomlineTitleLabel)
            make.left.equalTo(bottomlineTitleLabel.snp.right)
            make.right.equalTo(-15)
            make.width.equalTo(bottomlineTitleLabel)
            make.height.equalTo(20)
        }
        
        
        chartTitleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(bottomlineValueLabel.snp.bottom).offset(40)
            make.left.equalTo(titleLabel)
            make.bottom.equalTo(chartLineLabel.snp.top).offset(-15)
            make.height.equalTo(20)
        }
        chartPickerViewButton.snp.makeConstraints { (make) in
            make.top.equalTo(chartTitleLabel)
            make.right.equalTo(-15)
            make.height.equalTo(20)
            make.bottom.equalTo(chartLineLabel.snp.top).offset(-15)
        }
        chartLineLabel.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(chartTitleLabel.snp.bottom).offset(15)
            make.right.equalTo(-15)
            make.height.equalTo(1)
        }
        
        chartPickerViewButton?.rx.tap
            .bind { [weak self] in
                self?.showDateType()
            }.disposed(by: disposeBag)
        
        
        
        
    }
    
    func showDateType() {
        let pickerView = TTADataPickerView(title: "", type: .date, delegate: self)
        //        pickerView.type = .date
        //        /* Set `minimumDate` and `maximumDate`
        //         pickerView.minimumDate = Date(timeIntervalSinceNow: -24 * 60 * 60)
        //         pickerView.maximumDate = Date(timeIntervalSinceNow: 24 * 60 * 60)
        //         */
        //        pickerView.delegate = self
        pickerView.show {
            UIView.animate(withDuration: 0.3, animations: {
                self.view.backgroundColor = UIColor(white: 1.0, alpha: 0.96)
            })
        }
    }
    
    // MARK: - IndicatorInfoProvider
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
}
// MARK: - TTADataPickerViewDelegate

extension ChildExampleViewController: TTADataPickerViewDelegate {
    // when the pickerView type is `.text`, you clicked the done button, you will get the titles you selected just now from the `titles` parameter
    
    func dataPickerView(_ pickerView: TTADataPickerView, didSelectTitles titles: [String]) {
        //        showLabel.text = titles.joined(separator: " ")
    }
    // when the pickerView type is NOT `.text`, you clicked the done button, you will get the date you selected just now from the `date` parameters
    func dataPickerView(_ pickerView: TTADataPickerView, didSelectDate date: Date) {
        // actually you need configure the dateFormatter dateStyle and timeStyle to get the currect date from the `date` parameter
        if pickerView.type == .date {
            dateFormatter.dateStyle = .medium
            dateFormatter.timeStyle = .none
        } else if pickerView.type == .time {
            dateFormatter.timeStyle = .medium
            dateFormatter.dateStyle = .none
        } else if pickerView.type == .dateTime {
            dateFormatter.dateStyle = .medium
            dateFormatter.timeStyle = .medium
        }
        print(dateFormatter.string(from: date))
    }
    // when the pickerView  has been changed, this function will be called, and you will get the row and component which changed just now
    func dataPickerView(_ pickerView: TTADataPickerView, didChange row: Int, inComponent component: Int) {
        print(#function)
    }
    // when you clicked the cancel button, this function will be called firstly
    func dataPickerViewWillCancel(_ pickerView: TTADataPickerView) {
        print(#function)
    }
    // when you clicked the cancel button, this function will be called at the last
    func dataPickerViewDidCancel(_ pickerView: TTADataPickerView) {
        print(#function)
    }
}

